/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mcp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Khanza
 */
public class MCP {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Select Test Number = ");
//        selectNumber(scanner.nextInt());

        for (Integer i = 1; i <= 3; i++) {
            System.out.println("Number " + i);
            selectNumber(i);
            System.out.println();
            System.out.println();
        }

    }

    public static void selectNumber(Integer number) {
        Integer[] arr;
        String[] arrString;
        Integer x;
        switch (number) {
            case 1:
                System.out.println("==============================================");
                arr = new Integer[getRandomNumber(2, 9)];

                for (int i = 0; i < arr.length; i++) {
                    arr[i] = getRandomNumber(1, 99);
                }
                System.out.print("Input  = ");
                System.out.println(Arrays.toString(arr));
                System.out.println("==============================================");
                System.out.print("Output = ");
                System.out.println(Arrays.toString(number1(arr)));
                System.out.println("==============================================");
                break;
            case 2:
                System.out.println("==============================================");
                arr = new Integer[getRandomNumber(2, 9)];

                for (int i = 0; i < arr.length; i++) {
                    arr[i] = getRandomNumber(1, 99);
                }
                x = getRandomNumber(1, 99);
                System.out.print("Array  = ");
                System.out.println(Arrays.toString(arr));
                System.out.print("X      = ");
                System.out.println(x);
                System.out.println("==============================================");
                System.out.print("Output = ");
                System.out.println(Arrays.toString(number2(arr, x)));
                System.out.println("==============================================");
                break;
            case 3:
                System.out.println("==============================================");
                arrString = new String[getRandomNumber(2, 9)];

                for (int i = 0; i < arrString.length; i++) {
                    arrString[i] = getRandomString();
                }
                x = getRandomNumber(1, 10);
                System.out.print("Array  = ");
                System.out.println(Arrays.toString(arrString));
                System.out.print("X      = ");
                System.out.println(x);
                System.out.println("==============================================");
                System.out.print("Output = ");
                System.out.println(Arrays.toString(number3(arrString, x)));
                System.out.println("==============================================");
                break;
        }
    }

    public static Integer[] number1(Integer[] arr) {
        Integer max = arr[0];
        for (Integer i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        Integer[] newArr = new Integer[1];
        newArr[0] = max;
        return newArr;
    }

    public static Integer[] number2(Integer[] arr, Integer x) {
        List<Integer> list = new ArrayList<>();
        for (Integer i = 0; i < arr.length; i++) {
            if (x / arr[i] > 0 && x != arr[i]) {
                list.add(arr[i]);
            }
        }
        Integer[] newArr = new Integer[list.size()];
        list.toArray(newArr);
        return newArr;
    }

    public static String[] number3(String[] arr, Integer x) {
        List<String> list = new ArrayList<>();
        for (Integer i = 0; i < arr.length; i++) {
            if (arr[i].length() == x) {
                list.add(arr[i]);
            }
        }
        String[] newArr = new String[list.size()];
        list.toArray(newArr);
        return newArr;
    }

    public static Integer getRandomNumber(int min, int max) {
        Double ran = (Math.random() * (max - min)) + min;
        Integer random = ran.intValue();
        return random;
    }

    public static String getRandomString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < getRandomNumber(1, 20)) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

}
